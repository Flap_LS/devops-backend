

CREATE SEQUENCE IF NOT EXISTS spacemarine_seq START WITH 1 INCREMENT BY 1;

CREATE TABLE spacemarine
(
    id                BIGINT NOT NULL,
    name              VARCHAR(255),
    coordinate_x      INTEGER,
    coordinate_y      BIGINT,
    creation_date     TIMESTAMP with time zone,
    health            BIGINT,
    astartes_category VARCHAR(255),
    weapon_type       VARCHAR(255),
    melee_weapon      VARCHAR(255),
    CONSTRAINT pk_spacemarine PRIMARY KEY (id)
);

