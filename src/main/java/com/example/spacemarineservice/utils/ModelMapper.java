package com.example.spacemarineservice.utils;

import com.example.spacemarineservice.domain.SpaceMarine;
import com.example.spacemarineservice.models.Coordinates;
import com.example.spacemarineservice.models.SpaceMarineDto;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;


@Component
public class ModelMapper {

    public SpaceMarineDto map(SpaceMarine spaceMarine) {
        SpaceMarineDto dto = new SpaceMarineDto();
        dto.setId(spaceMarine.getId());
        dto.setName(spaceMarine.getName());
        dto.setHealth(spaceMarine.getHealth());
        dto.setCategory(spaceMarine.getCategory());
        dto.setWeaponType(spaceMarine.getWeaponType());
        dto.setMeleeWeapon(spaceMarine.getMeleeWeapon());
        dto.setCreationDate(spaceMarine.getCreationDate());
        dto.setCoordinates(Coordinates.of(spaceMarine.getCoordinateX(), spaceMarine.getCoordinateY()));

        return dto;
    }
}
