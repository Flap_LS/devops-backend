package com.example.spacemarineservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication

public class SpacemarineServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpacemarineServiceApplication.class, args);
    }

}
